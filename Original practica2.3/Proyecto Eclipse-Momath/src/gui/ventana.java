package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JRadioButton;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JToolBar;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JSlider;
import javax.swing.ButtonGroup;
import java.awt.Color;
import javax.swing.JList;
import javax.swing.JComboBox;
import javax.swing.JSpinner;
import javax.swing.JEditorPane;
import javax.swing.JTextPane;
import javax.swing.SpinnerNumberModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import java.awt.Toolkit;
import javax.swing.JTextArea;
import javax.swing.JFormattedTextField;
/**
 * 
 * @author momath
 * @since 17/01/2018
 *
 */
public class ventana extends JFrame {

	private JPanel contentPane;
	private JTextField textField_2;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();
	private final ButtonGroup buttonGroup_2 = new ButtonGroup();
	private final ButtonGroup buttonGroup_3 = new ButtonGroup();
	private JTextField textField_1;
	private JButton btnNewButton_3;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ventana frame = new ventana();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ventana() {
		setBackground(Color.WHITE);
		setIconImage(Toolkit.getDefaultToolkit().getImage(ventana.class.getResource("/images/descarga (1).jpg")));
		setTitle("Comprar Coche");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 674, 407);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnArchivo = new JMenu("Archivo");
		menuBar.add(mnArchivo);
		
		JMenu mnAbrir = new JMenu("abrir");
		mnArchivo.add(mnAbrir);
		
		JMenu mnInsertar = new JMenu("Insertar");
		menuBar.add(mnInsertar);
		
		JMenuItem mntmImgenes = new JMenuItem("Im\u00E1genes");
		mnInsertar.add(mntmImgenes);
		contentPane = new JPanel();
		contentPane.setBackground(Color.BLUE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JToolBar toolBar = new JToolBar();
		toolBar.setBackground(Color.LIGHT_GRAY);
		toolBar.setBounds(10, 11, 602, 31);
		contentPane.add(toolBar);
		
		JButton btnInicio = new JButton("Inicio");
		toolBar.add(btnInicio);
		
		JButton btnNewButton = new JButton("Guardar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		toolBar.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Guardar como");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		toolBar.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Cerrar");
		toolBar.add(btnNewButton_2);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(20, 53, 612, 283);
		contentPane.add(tabbedPane);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.CYAN);
		tabbedPane.addTab("Coches", null, panel, null);
		panel.setLayout(null);
		
		JButton btnModeloDelCoche = new JButton("Modelo del coche");
		btnModeloDelCoche.setForeground(Color.RED);
		btnModeloDelCoche.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnModeloDelCoche.setBounds(20, 102, 115, 23);
		panel.add(btnModeloDelCoche);
		
		JButton btnAo = new JButton("A\u00F1o");
		btnAo.setForeground(Color.MAGENTA);
		btnAo.setBounds(366, 102, 89, 23);
		panel.add(btnAo);
		
		textField_2 = new JTextField();
		textField_2.setBounds(493, 103, 86, 20);
		panel.add(textField_2);
		textField_2.setColumns(10);
		
		JButton button = new JButton("Version");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		button.setBounds(20, 161, 89, 23);
		panel.add(button);
		
		textField_1 = new JTextField();
		textField_1.setBounds(168, 162, 86, 20);
		panel.add(textField_1);
		textField_1.setColumns(10);
		
		btnNewButton_3 = new JButton("Marca");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton_3.setBounds(20, 35, 89, 23);
		panel.add(btnNewButton_3);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setModel(new DefaultComboBoxModel(new String[] {"Audi", "BMW", "Citroen", "Ford", "Mercedes", "Opel", "Peugeot", "Toyota", "Volvo", "VW"}));
		comboBox_1.setBounds(168, 36, 97, 20);
		panel.add(comboBox_1);
		
		JComboBox comboBox_2 = new JComboBox();
		comboBox_2.setModel(new DefaultComboBoxModel(new String[] {"A3 Series", "A5 Series", "520 Gran turismo", "125 Coup\u00E9", "C4", "Berlingo", "Mustang", "Kuga", "Sprinter", "SL 320", "Calibra", "Sintra", "Partner", "206", "Auris", "Avalon", "440", "V90", "Passat", "Eos"}));
		comboBox_2.setBounds(168, 100, 97, 23);
		panel.add(comboBox_2);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.CYAN);
		tabbedPane.addTab("Extras", null, panel_1, null);
		panel_1.setLayout(null);
		
		JCheckBox checkBox = new JCheckBox("Sensor de aparcamiento");
		checkBox.setForeground(Color.BLUE);
		checkBox.setBounds(32, 203, 141, 23);
		panel_1.add(checkBox);
		
		JSpinner spinner = new JSpinner();
		spinner.setModel(new SpinnerNumberModel(new Short((short) 0), new Short((short) 0), new Short((short) 400000), new Short((short) 1)));
		spinner.setBounds(116, 39, 57, 41);
		panel_1.add(spinner);
		
		JButton btnKms = new JButton("Kms");
		btnKms.setBounds(10, 48, 89, 23);
		panel_1.add(btnKms);
		
		JButton btnPuertas = new JButton("Puertas");
		btnPuertas.setBounds(10, 133, 89, 23);
		panel_1.add(btnPuertas);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"2", "3", "4", "5"}));
		comboBox.setBounds(149, 133, 45, 23);
		panel_1.add(comboBox);
		
		JButton button_1 = new JButton("Combustible");
		button_1.setIcon(new ImageIcon(ventana.class.getResource("/images/descarga.jpg")));
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		button_1.setBounds(324, 11, 248, 129);
		panel_1.add(button_1);
		
		JRadioButton radioButton = new JRadioButton("Gasolina");
		buttonGroup.add(radioButton);
		radioButton.setBounds(335, 183, 109, 23);
		panel_1.add(radioButton);
		
		JRadioButton radioButton_1 = new JRadioButton("Diesel");
		buttonGroup.add(radioButton_1);
		radioButton_1.setBounds(477, 183, 109, 23);
		panel_1.add(radioButton_1);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(Color.CYAN);
		tabbedPane.addTab("Precios", null, panel_2, null);
		panel_2.setLayout(null);
		
		JButton btnPrecio = new JButton("Precio");
		btnPrecio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnPrecio.setBounds(69, 164, 89, 23);
		panel_2.add(btnPrecio);
		
		JSlider slider = new JSlider();
		slider.setMinimum(100);
		slider.setMaximum(550);
		slider.setMajorTickSpacing(100);
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		slider.setBounds(10, 63, 200, 53);
		panel_2.add(slider);
		
		JButton btnPotenciaCv = new JButton("Potencia CV");
		btnPotenciaCv.setBounds(34, 29, 106, 23);
		panel_2.add(btnPotenciaCv);
		
		JSlider slider_1 = new JSlider();
		slider_1.setMaximum(55000);
		slider_1.setMinimum(5000);
		slider_1.setMajorTickSpacing(10000);
		slider_1.setPaintTicks(true);
		slider_1.setPaintLabels(true);
		slider_1.setBounds(10, 198, 263, 46);
		panel_2.add(slider_1);
		
		JFormattedTextField frmtdtxtfldEditor = new JFormattedTextField();
		frmtdtxtfldEditor.setBounds(505, 197, 68, 20);
		panel_2.add(frmtdtxtfldEditor);
		
		JButton btnTexto = new JButton("Texto");
		btnTexto.setBounds(377, 196, 89, 23);
		panel_2.add(btnTexto);
		
		JTextArea textArea = new JTextArea();
		textArea.setBounds(354, 28, 231, 150);
		panel_2.add(textArea);
		
		JTextPane textPane = new JTextPane();
		textPane.setBounds(447, 96, 79, 20);
		panel_2.add(textPane);
	}
}
