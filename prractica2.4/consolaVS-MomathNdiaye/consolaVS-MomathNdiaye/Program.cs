﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace consolaVS_MomathNdiaye
{
    class Program
    {
        static void Main(string[] args)
        {
            long cantidad = 3000;
            int opcion = 0;

            do
            {
                Console.WriteLine("1- INGRESAR");
                Console.WriteLine("2- RETIRAR");
                Console.WriteLine("3- CONSULTAR SALDO");
                Console.WriteLine("4- SALIR");
                Console.WriteLine("Pulsar opcion..X");
                opcion = int.Parse(Console.ReadLine());

                if (opcion == 1)
                {
                    Console.WriteLine(IngresarCantidad(cantidad));
                }

                if (opcion == 2)
                {
                    Console.WriteLine(RetirarCantidad(cantidad));


                }
                if (opcion == 3)
                {
                    Console.WriteLine(ConsultarCantidad(cantidad));

                }


                if (opcion == 4)
                {
                    Console.WriteLine(Salir(opcion));
                }


            } while (opcion != 4);
		
        }

        private static int Salir(int opcion)
        {
            Console.WriteLine("Salir");
            return opcion;

        }

        private static long ConsultarCantidad(long cantidad)
        {
            Console.WriteLine("Su saldo es " + cantidad);
            if (cantidad <= 0)
            {
                Console.WriteLine("No hay dinero");
            }
            return cantidad;
        }

        private static long RetirarCantidad(long cantidad)
        {
            long retiro;
            Console.WriteLine("¿Cuánto va a retirar?");
            retiro = long.Parse(Console.ReadLine());
            cantidad = cantidad - retiro;
            if (cantidad <= 0)
            {
                Console.WriteLine("Saldo 0");
            }
            else
            {
                Console.WriteLine("Su saldo es " + cantidad);
            }
            return cantidad;
        }

        private static long IngresarCantidad(long cantidad)
        {
            long ingreso;
            Console.WriteLine("¿Cuánto quiere ingresar?");
            ingreso = long.Parse(Console.ReadLine());
           cantidad= cantidad + ingreso;
            Console.WriteLine("Su saldo actual es de " + cantidad);
            return cantidad;
        }

    }
}

