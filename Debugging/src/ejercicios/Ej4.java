package ejercicios;

import java.util.Scanner;

public class Ej4 {

	public static void main(String[] args) {
		/*Establecer un breakpoint en la primera instrucción y avanzar
		 * instrucción a instrucción (step into) analizando el contenido de las variables
		 */
		
		Scanner lector;
		int numeroLeido;
		int resultadoDivision;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();

		for(int i = numeroLeido; i >= 0 ; i--){
			resultadoDivision = numeroLeido / i;
			//el error se da cuando el i toma valor 0 y como cualquier numero entre 0 es 0 nos da error.
			//para solucionarlo en i>=0 hay que cambiarlo y poner  i>0 y nos leer�a hasta el 1 y.
			System.out.println("el resultado de la division es: " + resultadoDivision);
		}
		
		//
		
		
		lector.close();
	}

}
