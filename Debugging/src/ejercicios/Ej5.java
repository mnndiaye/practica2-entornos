package ejercicios;

import java.util.Scanner;

public class Ej5 {

	public static void main(String[] args) {
		/*
		 * Ayudate del debugger para entender qué realiza este programa
		 */
		
		
		Scanner lector;
		int numeroLeido;
		int cantidadDivisores = 0;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();
		
		// El programa me pide un numero y yo pongo el 4 por ejemplo. Ahora va dividiendo el 4 entre i=1 y si el cociente es 0 me suma 1 en la 
		//cantidadDivisores pero si no es 0 el cociente pues no se suma nada en la cantidadDivisores. Al final si la cantidadDivisores
		// es mayor que 2 nos imprimir� "No lo es" y si es menor de 2 nos imprimir� "Si lo es".
		//Como la cantidadDivisores del numero 4 es "=3" que es mayor que "2" me imprimir� "No lo es".
		
		
		for (int i = 1; i <= numeroLeido; i++) {
			if(numeroLeido % i == 0){
				cantidadDivisores++;
			}
		}
		
		if(cantidadDivisores > 2){
			System.out.println("No lo es");
		}else{
			System.out.println("Si lo es");
		}
		
		
		lector.close();
	}

}
